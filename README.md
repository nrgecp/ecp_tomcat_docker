# ECP RBOS/NBOS Docker-Compose

## Starting and Stopping Containers

Use the `docker-compose` command and specify the yaml file and environment file with the `-f` and `--env-file` options respectively: 

*  `docker-compose -f /opt/ecp_tomcat_docker/ecp-tomcat-prod-compose.yaml --env-file /opt/ecp_tomcat_docker/default.env up`
*  `docker-compose -f opt/ecp_tomcat_docker/ecp-tomcat-prod-compose.yaml --env-file /opt/ecp_tomcat_docker/default.env down`


## Development vs Production Compose File

There are two docker compose files in this repository. The only difference between the two is that `ecp-tomcat-dev.yaml` enables the option to attach a remove debugger by exposing port `8000` and adding the `-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n` argument to `CATALINA_OPTS`.

## Environment File

The docker compose file is parameterized such that it can be used to run NBOS and RBOS. This environment file is specified when the `docker-compose` command is run using the `--env-file` option. An example environment file is shown below: 
```
UID=1001
GID=1001
WAR_NAME="CTT.war"
WAR_HOST_PATH="./tomcat-overlay/webapps"
VOLUME_PATH="/data/ecp-rbos"
TOMCAT_HEAPS="-Xmx2048m -Xms512m"
TOMCAT_IMAGE="unidata/tomcat-docker:8.5"
```
To get the values of `UID` and `GID`, you can run the command `id <USERNAME>` to find the id of the user and the id of the `nrg-ecp-fs` group. For example, if you are running Docker using the `nrg-svc-ecpnb` user, run the command `id nrg-svc-ecpnb` to find the user id.